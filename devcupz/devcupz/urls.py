from django.contrib import admin
from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView

from tastypie.api import Api

from foods.api import EntryResource, FoodResource, NutrientResource

foodz_api = Api(api_name='foodz')
foodz_api.register(FoodResource())
foodz_api.register(NutrientResource())
foodz_api.register(EntryResource())

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', TemplateView.as_view(template_name='base.html')),

    # Examples:
    # url(r'^$', 'devcupz.views.home', name='home'),
    # url(r'^devcupz/', include('devcupz.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include(foodz_api.urls)),
)
