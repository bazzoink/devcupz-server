import datetime
import json

from django.conf.urls.defaults import *
from django.http import HttpResponse

from tastypie import fields
from tastypie.authorization import DjangoAuthorization, Authorization
from tastypie.resources import ModelResource
from tastypie.utils import trailing_slash
from tastypie.utils.mime import determine_format, build_content_type

from core import get_query
from .models import Entry, Food, Nutrient

class FoodResource(ModelResource):

    class Meta:
        queryset = Food.objects.all()
        additional_detail_fields = {'nutrients': fields.ToManyField('foods.api.NutrientResource', 'nutrients', full=True)}
        authorization = Authorization()
        list_allowed_methods = ['get']


    def dehydrate(self, bundle):
        # detect if detail
        if self.get_resource_uri(bundle) == bundle.request.path:
            # detail detected, include additional fields
            bundle = self.detail_dehydrate(bundle)

        return bundle

    # detail_dehydrate is basically full_dehydrate
    # except we'll loop over the additional_detail_fields
    # and we won't want to do the dehydrate(bundle) at the end
    def detail_dehydrate(self, bundle):
        """
        Given a bundle with an object instance, extract the information from it
        to populate the resource.
        """
        # Dehydrate each field.
        # loop over additional_detail_fields instead
        #for field_name, field_object in self.fields.items():
        for field_name, field_object in self._meta.additional_detail_fields.items():
            # A touch leaky but it makes URI resolution work.
            if getattr(field_object, 'dehydrated_type', None) == 'related':
                field_object.api_name = self._meta.api_name
                field_object.resource_name = self._meta.resource_name

            bundle.data[field_name] = field_object.dehydrate(bundle)

            # Check for an optional method to do further dehydration.
            method = getattr(self, "dehydrate_%s" % field_name, None)

            if method:
                bundle.data[field_name] = method(bundle)

        # dehydrating the bundle will create an infinite loop
        #bundle = self.dehydrate(bundle)
        return bundle

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/search%s$" % (self._meta.resource_name,
                trailing_slash()), self.wrap_view('get_search'), name="api_get_search"),
        ]

    def get_search(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        # Do the query.
        query_string = ''
        found_entries = None
        if ('q' in request.GET) and request.GET['q'].strip():
            query_string = request.GET['q']
        entry_query = get_query(query_string, ['description'])

        found_entries = Food.objects.filter(entry_query).order_by('group', 'description')

        objects = []

        for result in found_entries:
            bundle = self.build_bundle(result, request=request)
            bundle = self.full_dehydrate(bundle)
            objects.append(bundle)

        object_list = {
            'objects': objects
        }

        self.log_throttled_access(request)
        return self.create_response(request, object_list)

class NutrientResource(ModelResource):
    food = fields.ForeignKey(FoodResource, 'food')

    class Meta:
        queryset = Nutrient.objects.all()

class EntryResource(ModelResource):
    food = fields.ForeignKey(FoodResource, 'food')

    class Meta:
        queryset = Entry.objects.all()
        list_allowed_methods = ['get', 'post']
        authorization = Authorization()

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/summary%s$" % (self._meta.resource_name,
                trailing_slash()), self.wrap_view('get_summary'), name="api_get_summary"),
        ]

    def get_summary(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        entries = None
        # get only entries from today
        today = datetime.date.today()
        datetime_range_today = [datetime.datetime.combine(today, datetime.time.min),
                                datetime.datetime.combine(today, datetime.time.max)]
        entries = Entry.objects.filter(created__range=datetime_range_today)

        nutrients = []
        value = 0
        for entry in entries:
            food_amount = entry.amount
            for nutrient in entry.food.nutrients.all():
                nutrients.append({'description': nutrient.description, 'group': nutrient.group, 
                    'units': nutrient.units, 'value': nutrient.value*food_amount/100})

        sum_counter = {}
        for x in nutrients:
            count = sum_counter.get(x['description'], 0)
            count += x['value']
            sum_counter[x['description']] = count
            x.pop('value')

        for x in nutrients:
            x['value'] = sum_counter[x['description']]

        #uniqify and sort
        nutrients = [dict(t) for t in set([tuple(d.items()) for d in nutrients])]
        nutrients = sorted(nutrients, key=lambda k: k['description'])
        nutrients = sorted(nutrients, key=lambda k: k['group'])

        response_data = {}
        response_data['num_entries'] = entries.count()
        response_data['nutrients'] = nutrients

        self.log_throttled_access(request)

        desired_format = determine_format(request, self._meta.serializer, default_format=self._meta.default_format)
        return HttpResponse(json.dumps(response_data), content_type=build_content_type(desired_format))
