from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _

class Food(models.Model):
	usda_id = models.IntegerField()
	description = models.TextField(max_length=3000)
	group = models.CharField(max_length=500)

class Nutrient(models.Model):
    food = models.ForeignKey(Food, related_name='nutrients')
    value = models.FloatField()
    units = models.CharField(max_length=8)
    description = models.TextField(max_length=3000)
    group = models.CharField(max_length=500)

    class Meta:
        unique_together = ('food', 'description',)

class Entry(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    food = models.ForeignKey(Food, related_name='food')
    amount = models.IntegerField(max_length=8)

    class Meta:
        verbose_name = _('Entry')
        verbose_name_plural = _('Entries')


