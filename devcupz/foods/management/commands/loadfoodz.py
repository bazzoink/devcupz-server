import json
import os

from django.core.management.base import BaseCommand, CommandError

from foods.models import Food, Nutrient


class Command(BaseCommand):

    def handle(self, *args, **options):
        json_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), "foods.json")

        required_keys = [('Energy', 'kcal'), ('Protein', 'g'), ('Vitamin A', 'mcg'), ('Vitamin C', 'mg'), ('Thiamin', 'mg'), ('Riboflavin', 'mg'), ('Niacin', 'mg'), ('Folate', 'mcg'), ('Calcium', 'mg'), ('Iron', 'mg'), ('Magnesium', 'mg'), ('Phosphorus', 'mg'), ('Zinc', 'mg'), ('Selenium', 'mcg'), ('Fluoride', 'mg'), ('Manganese', 'mg'), ('Vitamin D', 'mcg'), ('Vitamin E', 'mg'), ('Vitamin K', 'mcg'), ('Vitamin B-6', 'mg'), ('Vitamin B-12', 'mcg'), ('Sodium', 'mg'), ('Chloride', 'mg'), ('Potassium', 'mg')]

        with open(json_file) as data_file:    
            data = json.load(data_file)

        for x in data:
            food = Food.objects.create(**{
                    'usda_id': x.get('id'),
                    'group': x['group'],
                    'description': ", ".join(x['description'].split(", ") + x['tags']),
            })
            food.save()

            for y in x['nutrients']:
                for z in required_keys:
                    if z[0] in y['description']:

                        #cleanup units!
                        if z[1] != x.units:
                            if y['description'] == 'Vitamin D' and y['units'] == 'IU':
                                #convert to mcg
                                y['value'] = y['value']/40
                            elif y['description'] == 'Vitamin A, IU'and y['units'] == 'IU':
                                #convert to mcg_RAE
                                y['value'] = y['value']*0.3
                            elif y['description'] == 'Energy' and y['units'] == 'kJ':
                                #convert to kcal
                                y['value'] = y['value']*0.239005736
                            elif y['description'] == 'Fluoride, F' and y['units'] == 'mcg':
                                y['value'] = y['value']/1000
                        y['description'] = z[0]
                        y['units'] = z[1]
                        
                        nutrient = Nutrient.objects.create(**{
                            'food': food,
                            'units': y['units'],
                            'group': y['group'],
                            'description': y['description'],
                            'value': y['value']
                        })
                        nutrient.save()