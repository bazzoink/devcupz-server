from django.contrib import admin

from .models import Entry, Food, Nutrient


admin.site.register(Entry)
admin.site.register(Food)
admin.site.register(Nutrient)
